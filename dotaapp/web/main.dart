import 'dart:html';

void main() {
  Element team = querySelector('#teams'); // looping text function TEAM
  team.children.addAll(dotaTeams().map(newTHT)); // looping text function TEAM
  

  Element player = querySelector('#players'); // looping text function Player
  player.children.addAll(dotaPlayers().map(newPL)); // looping text function Player
}

// looping text function Player
Iterable<String> dotaPlayers() sync* {
  var players = ['N0tail', 'Cr1t', 'Artsy', 'Dendi', 'Ana'];

  for (var player in players) {
    yield player;
  }
}
// looping text function Player
TableRowElement newPL(String playerText) => TableRowElement()..text = playerText;

// looping text function TEAM
Iterable<String> dotaTeams() sync* {
  var team = ['OG', 'Team Liquid', 'Evil Genius', 'PSG LGD'];

  for (var teams in team) {
    yield teams;
  }
}
// looping text function TEAM
TableRowElement newTHT(String teamText) => TableRowElement()..text = teamText;