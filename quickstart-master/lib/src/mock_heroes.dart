import 'hero.dart';

final mockHeroes = <Hero>[

Hero(1, "Slark"),
Hero(2, "Phantom Assassin"),
Hero(3, "Phantom Lancer"),
Hero(4, "Slardar"),
Hero(5, "Faceless Void"),
Hero(6, "Enchantress"),
Hero(7, "Ember Spirit"),
Hero(8, "Void Spirit"),
Hero(9, "Earth Spirit"),
Hero(10, "Storm Spirit"),

];